--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")

local MAX_TEXT_LENGTH = 45

local managers = mod:persistent_table("managers", {})

mod:dofile("scripts/mods/social_wheel_improvements/social_wheel/settings_manager")
managers.settings = SocialWheelUISettingsManager:new()

mod:dofile("scripts/mods/social_wheel_improvements/social_wheel/social_wheel_ui")

local social_ui_component_definition = {
  always_active = true,
  use_hud_scale = true,
  class_name = "SocialUI",
  filename = "scripts/mods/social_wheel_improvements/social_ui/social_ui",
  visibility_groups = {
    "dead",
    "alive",
    "realism",
    "tab_menu",
    "cutscene",
    "gift_popup",
    "in_menu",
    "in_endscreen",
    "mission_vote",
    "hero_selection_popup",
    "entering_mission",
  },
}

mod:dofile(social_ui_component_definition.filename)
local ingame_hud_definitions = require("scripts/ui/views/ingame_hud_definitions")
table.insert(ingame_hud_definitions.components, 1, social_ui_component_definition)
ingame_hud_definitions.components_lookup[social_ui_component_definition.class_name] = social_ui_component_definition
local visibility_groups_lookup = ingame_hud_definitions.visibility_groups_lookup
ingame_hud_definitions.components_hud_scale_lookup[social_ui_component_definition.class_name] = true

for _, group in ipairs(social_ui_component_definition.visibility_groups) do
  visibility_groups_lookup[group].visible_components[social_ui_component_definition.class_name] = true
end

mod:hook_safe(IngameHud, "init", function(self)
  managers.social_ui = self:component(social_ui_component_definition.class_name)
  managers.settings:load()
end)

function mod.on_unload()
  if managers.social_wheel_ui then
    managers.social_wheel_ui:reset()
  end
end

local function action_add(...)
  local text = table.concat({ ... }, " ")

  if string.len(text) > MAX_TEXT_LENGTH then
    mod:error("The text exceeds the maximum of %s characters.")
    return
  end

  managers.settings:add_setting("general", { text = text })
  managers.settings:save()

  if managers.social_wheel_ui then
    managers.social_wheel_ui:reset()
  end
end

local function action_remove(index)
  managers.settings:remove_setting("general", tonumber(index))
  managers.settings:save()

  if managers.social_wheel_ui then
    managers.social_wheel_ui:reset()
  end
end

local function action_reset()
  managers.settings:reset()
  managers.settings:delete()

  if managers.social_wheel_ui then
    managers.social_wheel_ui:reset()
  end
end

local function show_help()
  mod:echo(
    "/social_wheel <action>\n\n" ..
    "Available actions:\n" ..
    "help                        Show this help message\n" ..
    "add <text>             Add a new message to the wheel\n" ..
    "remove <index>   Remove the message at the given index\n" ..
    "reset                      Reset Social Wheel to default"
  )
end

mod:command("social_wheel", "Configure the social wheel", function(action, ...)
  if not managers.social_ui then
    mod:warning("SocialUI has not been initialized")
  end

  if action == "add" then
    action_add(...)
  elseif action == "remove" then
    action_remove(...)
  elseif action == "reset" then
    action_reset(...)
  else
    show_help(...)
  end
end)
