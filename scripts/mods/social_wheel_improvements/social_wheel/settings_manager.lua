--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")
local SAVE_FILE = "social_wheel_improvements/data.db"

SocialWheelUISettingsManager = class(SocialWheelUISettingsManager)

local MAX_CATEGORY_SETTINGS = 10

local function generate_id(category_name)
  return string.format("social_wheel_%s_%s", category_name, os.time())
end

function SocialWheelUISettingsManager:init()
  self._priority = {}
  self._settings = {}
  self._lookup = {}

  self.save_manager = SaveManager:new(false)

  self:reset()
end

function SocialWheelUISettingsManager:reset()
  self._priority = table.clone(SocialWheelPriority)

  local function clone_setting_table(setting)
    setting = table.clone(setting)
    setting.text = Localize(setting.text)

    if type(setting.event_text) == "string" then
      setting.event_text = Localize(setting.event_text)
    end

    return setting
  end

  self._settings = {}

  for category_name, category_settings in pairs(SocialWheelSettings) do
    local settings = {}
    self._settings[category_name] = settings

    for key, setting in pairs(category_settings) do
      if type(key) == "number" then
        setting = clone_setting_table(setting)
      end

      settings[key] = setting
    end
  end

  self._lookup = {}

  for setting_name, setting in pairs(SocialWheelSettingsLookup) do
    self._lookup[setting_name] = clone_setting_table(setting)
  end
end

function SocialWheelUISettingsManager:add_category(category, priority)
  local category_name = category.name
  if self._settings[category_name] then
    mod:error("Category '%s' already exists.", category_name)
    return
  end

  category.size = category.size or { 250, 250 }
  category.wedge_adjustment = category.wedge_adjustment or 0.85

  self._settings[category_name] = category

  if priority then
    local priority_index = priority.index
    if not priority_index or priority_index < 1 or priority_index > #self._priority then
      priority_index = #self._priority
    end

    table.insert(self._priority, priority_index, {
      category.name,
      priority.condition_func,
    })
  end
end

function SocialWheelUISettingsManager:add_setting(category_name, setting, force)
  local category_settings = self._settings[category_name]
  local setting_name = setting.name or generate_id(category_name)
  if not category_settings then
    mod:error("Category '%s' doesn't exist. Make sure to add it first.", category_name)
    return
  elseif self._lookup[setting_name] and not force then
    mod:error("Setting '%s' already exists.", setting_name)
    return
  end

  local num_settings = #category_settings

  if num_settings > MAX_CATEGORY_SETTINGS then
    mod:error("Only %d settings per category allowed.", MAX_CATEGORY_SETTINGS)
    return
  end

  if not setting.event_text then
    if type(setting.text) == "function" then
      setting.event_text_func = setting.text
    else
      setting.event_text = setting.text
    end
  end

  setting.name = setting_name
  setting.index = num_settings
  setting.data = setting.data or {}
  setting.icon = setting.icon or "radial_chat_icon_ready"

  table.insert(category_settings, setting)
  self._lookup[setting_name] = setting
end

function SocialWheelUISettingsManager:remove_setting(category_name, index)
  local category_settings = self:get_category(category_name)
  local setting = category_settings[index]

  if not setting then
    mod:error("Setting at index %s doesn't exist in category %s", index, category_name)
    return
  end

  self._lookup[setting.name] = nil
  table.remove(category_settings, index)
end

function SocialWheelUISettingsManager:get_setting(setting_name)
  return self._lookup[setting_name]
end

function SocialWheelUISettingsManager:get_category(category_name)
  return self._settings[category_name]
end

function SocialWheelUISettingsManager:get_all_categories()
  return self._settings
end

function SocialWheelUISettingsManager:get_priorities()
  return self._priority
end

function SocialWheelUISettingsManager:load()
  self.save_manager:auto_load(SAVE_FILE, callback(self._on_data_loaded, self))
end

function SocialWheelUISettingsManager:_on_data_loaded(info)
  -- A horrible hardcoded override for now, as the way how these settings are stored and accessed by the SocialWheelUI
  -- doesn't allow for a nice, clean replacement/insertion
  self._settings.general = {
    wedge_adjustment = 0.85,
    individual_bg = true,
    angle = 1.7 * math.pi,
    size = {
      500,
      250
    },
  }

  if info.error then
    self:reset()
  elseif info.data then
    local data = info.data.general

    for _, setting in ipairs(data) do
      self:add_setting("general", table.clone(setting), true)
    end
  end
end

function SocialWheelUISettingsManager:save()
  local general_settings = {}

  for _, setting in ipairs(self:get_category("general")) do
    table.insert(general_settings, table.clone(setting))
  end

  local data = {
    general = general_settings,
  }

  self.save_manager:auto_save(SAVE_FILE, data)
end

function SocialWheelUISettingsManager:delete()
  Cloud.delete(SAVE_FILE)
end
