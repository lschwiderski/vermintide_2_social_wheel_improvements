--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")

local MAX_CATEGORY_SETTINGS = 10

local function func_and(...)
  local num_funcs = select("#", ...)
  local funcs = {...}

  return function(...)
    local ret = true

    for i = 1, num_funcs do
      local func = funcs[i]
      ret = ret and func(...)
    end

    return ret
  end
end

local function fetch_player_from_profile_display_name(wanted_profile_name)
	local player_manager = Managers.player
	local players = player_manager:players()

	for _, player in pairs(players) do
		local profile_display_name = player:profile_display_name()

		if profile_display_name == wanted_profile_name then
			return player
		end
	end
end

local function is_unit_alive(_, active_context)
  local target_unit = active_context.unit

	return Unit.alive(target_unit)
end

local function get_pickup_event_text(target_unit, event_settings)
	local callee_profile_name = event_settings.data
	local callee_player = fetch_player_from_profile_display_name(callee_profile_name)

	if callee_player then
		local pickup_extension = ScriptUnit.extension(target_unit, "pickup_system")
		local settings = pickup_extension:get_pickup_settings()

		local profile_index = callee_player:profile_index()
		local profile = SPProfiles[profile_index]

		if settings.type == "ammo" then
      local event_text_id = "social_wheel_pickup_item_ammo_event"

      return string.format(Localize(event_text_id), profile.ingame_short_display_name)
		else
			local event_text_id = "social_wheel_pickup_item_event"
      local item_name = Unit.get_data(target_unit, "interaction_data", "hud_description")

      return string.format(Localize(event_text_id), profile.ingame_short_display_name, item_name)
		end
	end
end

local function get_drop_event_text(target_unit, event_settings)
	local player_manager = Managers.player
	local callee_player = player_manager:owner(target_unit)

	if callee_player then
    local event_text_id = "social_wheel_player_drop_event"

		local item_name = event_settings.data
    local item_data = AllPickups[item_name]

		local profile_index = callee_player:profile_index()
		local profile = SPProfiles[profile_index]

		return string.format(Localize(event_text_id), item_data.hud_description, profile.ingame_short_display_name)
	end
end

local function pickup_item(callee_profile_name, item_unit, caller_player, settings)
	local callee_player = fetch_player_from_profile_display_name(callee_profile_name)

	if callee_player then
		local callee_player_unit = callee_player.player_unit
		local ai_bot_group_system = Managers.state.entity:system("ai_bot_group_system")

		ai_bot_group_system:order("pickup", callee_player_unit, item_unit, caller_player)

		local player_unit = caller_player.player_unit

		if settings.ping and Unit.alive(item_unit) and Unit.alive(player_unit) then
			local network_manager = Managers.state.network
			local pinger_unit_id = network_manager:unit_game_object_id(player_unit)
			local pinged_unit_id = network_manager:unit_game_object_id(item_unit)

			network_manager.network_transmit:send_rpc_server(
        "rpc_ping_unit_exclusive_flash",
        pinger_unit_id,
        pinged_unit_id,
        callee_player:network_id(),
        callee_player:local_player_id()
      )
		end
	end
end

local function drop_item(item_name, callee_player_unit, caller_player)
	local ai_bot_group_system = Managers.state.entity:system("ai_bot_group_system")

	ai_bot_group_system:order("drop", callee_player_unit, item_name, caller_player)
end

local function has_item(item_name, active_context)
	local player_unit = active_context.unit

	local item = AllPickups[item_name]
	local slot_name = item.slot_name
	local inventory_extension = ScriptUnit.has_extension(player_unit, "inventory_system")
	local slot_data = inventory_extension:get_slot_data(slot_name)

	if slot_data then
		local item_template = inventory_extension:get_item_template(slot_data)

		if item_name == "grimoire" then
			return item_template.is_grimoire
		else
			local pickup_data = item_template.pickup_data

			return pickup_data.pickup_name == item_name
		end
	else
		return false
	end
end

local function is_valid_player_and_target_unit_exclude_local_player(player_profile_name, active_context)
	local target_unit = active_context.unit

	if not Unit.alive(target_unit) then
		return false
	end

	local player_manager = Managers.player
	local local_player = player_manager:local_player()
	local player = fetch_player_from_profile_display_name(player_profile_name)

	if not player or player == local_player or not Unit.alive(player.player_unit) then
		return false
	end

	local status_extension = ScriptUnit.extension(player.player_unit, "status_system")

	if status_extension:is_ready_for_assisted_respawn() or status_extension:is_dead() then
		return false
	end

	local is_bot = not player:is_player_controlled()
	local pickup_extension = ScriptUnit.extension(target_unit, "pickup_system")
	local settings = pickup_extension:get_pickup_settings()

	if is_bot and settings.slot_name == "slot_level_event" then
		return false
	else
		return true
	end
end

local function is_bot_player(player_profile_name)
  local player = fetch_player_from_profile_display_name(player_profile_name)
  return not player:is_player_controlled()
end

SocialWheelPriority = {}
SocialWheelSettings = {}
SocialWheelSettingsLookup = {}

function mod:add_category(category, priority)
  local category_name = category.name
  if SocialWheelSettings[category_name] then
    mod:error("Category '%s' already exists.", category_name)
    return
  end

  category.size = category.size or { 250, 250 }
  category.wedge_adjustment = category.wedge_adjustment or 0.85

  SocialWheelSettings[category_name] = category

  if priority then
    local priority_index = priority.index
    if not priority_index or priority_index < 1 or priority_index > #SocialWheelPriority then
      priority_index = #SocialWheelPriority
    end

    table.insert(SocialWheelPriority, priority_index, {
      category.name,
      priority.condition_func,
    })
  end
end

local function generate_id(category_name)
  return string.format("social_wheel_%s_%s", category_name, os.time())
end

function mod:add_setting(category_name, setting)
  local category_settings = SocialWheelSettings[category_name]
  local setting_name = setting.name or generate_id(category_name)
  if not category_settings then
    mod:error("Category '%s' doesn't exist. Make sure to add it first.", category_name)
    return
  elseif SocialWheelSettingsLookup[setting_name] then
    mod:error("Setting '%s' already exists.", setting_name)
    return
  end

  local num_settings = #category_settings

  if num_settings > MAX_CATEGORY_SETTINGS then
    mod:error("Only %d settings per category allowed.", MAX_CATEGORY_SETTINGS)
    return
  end

  setting.name = setting_name
  setting.index = num_settings
  setting.data = setting.data or {}
  setting.icon = setting.icon or "radial_chat_icon_ready"

  table.insert(category_settings, setting)
  SocialWheelSettingsLookup[setting_name] = setting
end

mod:add_category({
  name = "general",
  individual_bg = true,
  angle = 1.7 * math.pi,
  size = {
    500,
    250
  },
})

mod:add_category({
  name = "general_gamepad",
  angle = 2 * math.pi,
})

mod:add_category(
  {
    name = "item",
    wedge_adjustment = 0.9,
    ping = true,
    angle = math.pi * 2,
  },
  {
    condition_func = function (_, _, social_wheel_unit)
			return social_wheel_unit and ScriptUnit.has_extension(social_wheel_unit, "pickup_system")
		end,
  }
)

mod:add_category(
  {
    name = "player",
    wedge_adjustment = 1,
    ping = false,
    angle = math.pi,
  },
  {
    condition_func = function (_, _, social_wheel_unit)
			return social_wheel_unit and Managers.player:is_player_unit(social_wheel_unit)
		end,
  }
)


mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_no"),
  text = Localize("social_wheel_general_no_gp"),
  name = "social_wheel_general_no_gp",
  icon = "radial_chat_icon_no",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_come_here"),
  text = Localize("social_wheel_general_come_here_gp"),
  name = "social_wheel_general_come_here_gp",
  icon = "radial_chat_icon_come_here",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_patrol"),
  text = Localize("social_wheel_general_patrol_gp"),
  name = "social_wheel_general_patrol_gp",
  icon = "radial_chat_icon_patrol",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_help"),
  text = Localize("social_wheel_general_help_gp"),
  name = "social_wheel_general_help_gp",
  icon = "radial_chat_icon_help",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_boss"),
  text = Localize("social_wheel_general_boss_gp"),
  name = "social_wheel_general_boss_gp",
  icon = "radial_chat_icon_boss",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_thank_you"),
  text = Localize("social_wheel_general_thank_you_gp"),
  name = "social_wheel_general_thank_you_gp",
  icon = "radial_chat_icon_thank_you",
})

mod:add_setting("general_gamepad", {
  event_text = Localize("social_wheel_general_yes"),
  text = Localize("social_wheel_general_yes_gp"),
  name = "social_wheel_general_yes_gp",
  icon = "radial_chat_icon_yes",
})


mod:add_setting("item", {
  text = Localize("social_wheel_item_pick_up_witch_hunter"),
  name = "social_wheel_item_pick_up_witch_hunter",
  icon = "radial_chat_icon_saltzpyre",
  data = "witch_hunter",
  event_text_func = get_pickup_event_text,
  execute_func = pickup_item,
  is_valid_func = is_valid_player_and_target_unit_exclude_local_player
})

mod:add_setting("item", {
  text = Localize("social_wheel_item_pick_up_bright_wizard"),
  name = "social_wheel_item_pick_up_bright_wizard",
  icon = "radial_chat_icon_sienna",
  data = "bright_wizard",
  event_text_func = get_pickup_event_text,
  execute_func = pickup_item,
  is_valid_func = is_valid_player_and_target_unit_exclude_local_player
})

mod:add_setting("item", {
  text = Localize("social_wheel_item_pick_up_dwarf_ranger"),
  name = "social_wheel_item_pick_up_dwarf_ranger",
  icon = "radial_chat_icon_bardin",
  data = "dwarf_ranger",
  event_text_func = get_pickup_event_text,
  execute_func = pickup_item,
  is_valid_func = is_valid_player_and_target_unit_exclude_local_player
})

mod:add_setting("item", {
  text = Localize("social_wheel_item_pick_up_wood_elf"),
  name = "social_wheel_item_pick_up_wood_elf",
  icon = "radial_chat_icon_kerillian",
  data = "wood_elf",
  event_text_func = get_pickup_event_text,
  execute_func = pickup_item,
  is_valid_func = is_valid_player_and_target_unit_exclude_local_player
})

mod:add_setting("item", {
  text = Localize("social_wheel_item_pick_up_empire_soldier"),
  name = "social_wheel_item_pick_up_empire_soldier",
  icon = "radial_chat_icon_kruber",
  data = "empire_soldier",
  event_text_func = get_pickup_event_text,
  execute_func = pickup_item,
  is_valid_func = is_valid_player_and_target_unit_exclude_local_player
})


mod:add_setting("player", {
  text = Localize("social_wheel_player_drop_grimoire"),
  name = "social_wheel_player_drop_grimoire",
  icon = "radial_chat_icon_drop_grimoire",
  data = "grimoire",
  event_text_func = get_drop_event_text,
  execute_func = drop_item,
  is_valid_func = func_and(is_unit_alive, has_item),
})

local function apply_default_general_category()
  mod:add_setting("general", {
    text = Localize("social_wheel_general_no"),
    name = "social_wheel_general_no",
    icon = "radial_chat_icon_no",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_come_here"),
    name = "social_wheel_general_come_here",
    icon = "radial_chat_icon_come_here",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_patrol"),
    name = "social_wheel_general_patrol",
    icon = "radial_chat_icon_patrol",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_help"),
    name = "social_wheel_general_help",
    icon = "radial_chat_icon_help",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_boss"),
    name = "social_wheel_general_boss",
    icon = "radial_chat_icon_boss",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_thank_you"),
    name = "social_wheel_general_thank_you",
    icon = "radial_chat_icon_thank_you",
  })

  mod:add_setting("general", {
    text = Localize("social_wheel_general_yes"),
    name = "social_wheel_general_yes",
    icon = "radial_chat_icon_yes",
  })
end

return {
  apply_default_general_category = apply_default_general_category,
}
