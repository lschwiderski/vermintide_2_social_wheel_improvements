--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")

-- TODO: Move to mod options
local SEND_CHAT_MESSAGE = true

local PLATFORM = PLATFORM
local STOP_LERP_TIME = 0.125
local STOP_LERP_TIME_CONTROLLER = 0.25
local ANIMATION_TIMES = {
  OPEN = {
    MOVE_Y = 1.5,
    SIZE = 1.5,
    MOVE_X = 1.5,
    ALPHA = 0.45
  },
  CLOSE = {
    MOVE_Y = 0.2,
    SIZE = 0.25,
    MOVE_X = 0.2,
    ALPHA = 0.1
  }
}

local managers = mod:persistent_table("managers")

local definitions = require("scripts/ui/social_wheel_ui_definitions")
local scenegraph_definition = definitions.scenegraph_definition

local function rpc_social_wheel_message(sender, message)
  local sender_player

  for _, player in pairs(Managers.player:human_players()) do
    if player.peer_id == sender then
      sender_player = player
    end
  end

  if not sender_player then
    mod:error("Couldn't find player for peer_id '%s'", sender)
    return
  end

  managers.social_ui:add_message(sender_player, message)
end

mod:network_register("social_wheel_message", rpc_social_wheel_message)

mod:hook(definitions, "create_social_widget", function(func, ...)
  local widget_data = func(...)

  widget_data.style.text.localize = false
  widget_data.style.text_shadow.localize = false

  return widget_data
end)

mod:hook_safe(SocialWheelUI, "init", function(self)
  managers.social_wheel_ui = self
end)

mod:hook_origin(SocialWheelUI, "_add_social_wheel_event", function(self, player, event, target_unit)
  local event_settings = managers.settings:get_setting(event)
	local event_text = event_settings.event_text or event_settings.text
	local text_func = event_settings.event_text_func

	if text_func then
    event_text = text_func(target_unit, event_settings)
  end

	if event_text then
    if self._num_free_events >= 1 then
      mod:network_send("social_wheel_message", (mod:get("send_to_self") and "all") or "others", event_text)

      if SEND_CHAT_MESSAGE then
        Managers.chat:send_chat_message(1, player:local_player_id(), event_text, false, nil, false)
      end
    else
      mod:error(Localize("social_wheel_too_many_messages_warning"))
    end
	end
end)

mod:hook_origin(SocialWheelUI, "_create_ui_elements", function (self)
	self._ui_scenegraph = UISceneGraph.init_scenegraph(scenegraph_definition)
	self._animations = {}
	self._animation_callbacks = {}
	self._selection_widgets = {}
	self._social_event_widgets = {}
	self._queued_social_wheel_events = {}
	self._icon_widgets = {}
	self._event_index = 0
	self._select_timer = 0
	self._selected_widget = nil

	local function get_active_context_func()
		return self._active_context
	end

	for category_name, category_settings in pairs(managers.settings:get_all_categories()) do
		local num_category_settings = #category_settings
		local category_widgets = Script.new_array(num_category_settings)
		self._selection_widgets[category_name] = category_widgets

		for i = 1, num_category_settings, 1 do
			local widget = definitions.create_social_widget(
        category_settings[i],
        self:_widget_angle(category_settings.angle, num_category_settings, i),
        category_settings,
        get_active_context_func
      )
			category_widgets[i] = UIWidget.init(widget)
		end
	end

	self._arrow_widget = UIWidget.init(definitions.arrow_widget)
	self._bg_widget = UIWidget.init(definitions.create_bg_widget())

	UIRenderer.clear_scenegraph_queue(self._ui_top_renderer)
end)

mod:hook_origin(SocialWheelUI, "_open_menu", function (self, _, t)
	local animation_times = ANIMATION_TIMES.OPEN
  local animations = self._animations

	animations.update_alpha = UIAnimation.init(
    UIAnimation.function_by_time,
    self._render_settings,
    "alpha_multiplier",
    0,
    1,
    animation_times.ALPHA,
    math.easeOutCubic
  )

	self._active_context = self._current_context
	local active_context = self._active_context
	local social_wheel_unit = active_context.unit

	if not Unit.alive(social_wheel_unit) then
		social_wheel_unit = nil
	end

	local category = "general"
  local priorities = managers.settings:get_priorities()

	for i = 1, #priorities, 1 do
		local data = priorities[i]
		local selected_category = data[1]
		local condition_function = data[2]

		if condition_function(active_context, self._player, social_wheel_unit) then
			category = selected_category
			break
		end
	end

	self._current_selection_widgets = self._selection_widgets[category]
	local settings = managers.settings:get_category(category)

	fassert(settings, "No settings for category %q.", category)

	self._current_selection_widget_settings = settings
	local selection_widgets = self._current_selection_widgets
	local num_selection_widgets = #selection_widgets

	for i = 1, num_selection_widgets, 1 do
		local widget = selection_widgets[i]
		local widget_content = widget.content
		local final_offset = widget_content.final_offset
		local dir = widget_content.dir:unbox()
    local offset = widget.offset

		animations["animation_x_" .. i] = UIAnimation.init(
      UIAnimation.function_by_time,
      offset,
      1,
      dir[1] * final_offset[1] * 0.5,
      dir[1] * final_offset[1],
      animation_times.MOVE_X,
      math.ease_out_elastic
    )

		animations["animation_y_" .. i] = UIAnimation.init(
      UIAnimation.function_by_time,
      offset,
      2,
      dir[2] * final_offset[2] * 0.5,
      dir[2] * final_offset[2],
      animation_times.MOVE_Y,
      math.ease_out_elastic
    )

		animations["animation_divider_size_" .. i] = UIAnimation.init(
      UIAnimation.function_by_time,
      widget_content,
      "size_multiplier",
      widget_content.final_size_multiplier * 0.5,
      widget_content.final_size_multiplier,
      animation_times.SIZE,
      math.ease_out_elastic
    )
	end

	local bg_widget = self._bg_widget
  local widget_content = bg_widget.content

	animations.animation_bg_size = UIAnimation.init(
    UIAnimation.function_by_time,
    widget_content,
    "size_multiplier",
    widget_content.final_size_multiplier * 0.5,
    widget_content.final_size_multiplier,
    animation_times.SIZE,
    math.ease_out_elastic
  )

	local gamepad_enabled = Managers.input:is_device_active("gamepad")
	local stop_lerp_time = (gamepad_enabled and STOP_LERP_TIME_CONTROLLER) or STOP_LERP_TIME
	self._selected_widget = nil
	self._open_start_t = t

	self:_set_player_input_scale(0, stop_lerp_time)
	self:_change_state("update_open")
end)

function SocialWheelUI:reset()
	self._render_settings = {
		alpha_multiplier = 0
	}
	self._state = "update_closed"
	self._states = {
		update_closed = true,
		update_open = true
	}

	self._current_context = nil
	self._active_context = nil
	self._num_free_events = 5
  self._last_update_t = 0

  self:_create_ui_elements()
end
