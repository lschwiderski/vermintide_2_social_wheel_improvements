--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")

local definitions = mod:dofile("scripts/mods/social_wheel_improvements/social_ui/social_ui_definitions")
local scenegraph_definition = definitions.scenegraph_definition

local MAX_MESSAGE_TEXT_LENGTH = 45

SocialUI = class(SocialUI)

function SocialUI:init(parent, ingame_ui_context)
  self.parent = parent
  self.ingame_ui_context = ingame_ui_context

  self.player_manager = ingame_ui_context.player_manager
  self.input_manager = ingame_ui_context.input_manager
  self.player = ingame_ui_context.player

  self._wwise_world = ingame_ui_context.wwise_world
  self.ui_top_renderer = ingame_ui_context.ui_top_renderer
  self.render_settings = {
    snap_pixel_positions = true
  }

  self.messages = {}

  self:create_ui_elements()
end

function SocialUI:create_ui_elements()
  self.ui_scenegraph = UISceneGraph.init_scenegraph(scenegraph_definition)
  self.message_widgets = {}

  for _, widget in pairs(definitions.message_widgets) do
    self.message_widgets[#self.message_widgets + 1] = UIWidget.init(widget)
  end

  self.unused_widgets = table.clone(self.message_widgets)

  UIRenderer.clear_scenegraph_queue(self.ui_top_renderer)
end

function SocialUI:add_message(player, text)
  if string.len(text) > MAX_MESSAGE_TEXT_LENGTH then
    mod:error("The message exceeds the maximum length of %s", MAX_MESSAGE_TEXT_LENGTH)
    return
  end

  local messages = self.messages
  local t = Managers.time:time("game")
  -- TODO: Move to mod options
  local increment_duration = UISettings.positive_reinforcement.increment_duration
  local unused_widgets = self.unused_widgets

  if #unused_widgets == 0 then
    self:remove_message(#messages)
  end

  local widget = table.remove(unused_widgets, 1)

  -- Reset X offset for the fade-in animation
  widget.offset[1] = 0
  widget.content.text = text

  local profile_data = SPProfiles[player:profile_index()]
  local careers = profile_data.careers
  local career_data = careers[player:career_index()]
  local character_portrait = career_data.portrait_image

  widget.content.portrait.texture_id = "small_" .. character_portrait

  table.insert(messages, 1, {
    widget = widget,
    next_increment = t - increment_duration,
  })

  self:_play_sound("Play_hud_socialwheel_notification")
end

function SocialUI:remove_message(index)
  local messages = self.messages
  local message = table.remove(messages, index)
  local widget = message.widget
  local unused_widgets = self.unused_widgets
  unused_widgets[#unused_widgets + 1] = widget
end

function SocialUI:_play_sound(sound_event)
  WwiseWorld.trigger_event(self._wwise_world, sound_event)
end

function SocialUI:update(dt, t)
  local ui_renderer = self.ui_top_renderer
  local ui_scenegraph = self.ui_scenegraph
  local input_service = self.input_manager:get_service("Player")
  local render_settings = self.render_settings

  UIRenderer.begin_pass(ui_renderer, ui_scenegraph, input_service, dt, nil, render_settings)

  mod:pcall(function()
    -- TODO: Move to mod options
    local show_duration = UISettings.positive_reinforcement.show_duration
    local snap_pixel_positions = render_settings.snap_pixel_positions

    local messages = self.messages

    for index, message in ipairs(messages) do
      repeat
        local widget = message.widget
        local content = widget.content
        local style = widget.style
        local offset = widget.offset

        if not message.remove_time then
          message.remove_time = t + show_duration
        elseif message.remove_time < t then
          self:remove_message(index)
          break
        end

        local step_size = 80
        -- TODO: Add mod option to reverse the direction -> change `-`
        local new_height_offset = -((index - 1) * step_size)

        if new_height_offset < offset[2] then
          local speed = 400
          offset[2] = math.max(offset[2] - dt * speed, new_height_offset)
        else
          offset[2] = new_height_offset
        end

        local time_left = message.remove_time - t
        -- TODO: Move to mod options
        local fade_duration = UISettings.positive_reinforcement.fade_duration
        local fade_out_progress

        if fade_duration < time_left then
          fade_out_progress = math.clamp((show_duration - time_left) / fade_duration, 0, 1)
          -- TODO: Add mod option to reverse the direction -> change `-`. Tied to the "left aligned" option
          offset[1] = -(math.easeInCubic(1 - fade_out_progress) * 35)
        else
          fade_out_progress = math.clamp(time_left / fade_duration, 0, 1)
        end

        local anim_progress = math.easeOutCubic(fade_out_progress)
        local alpha = 255 * anim_progress
        local texte_style_ids = content.texte_style_ids

        for _, style_def in ipairs(texte_style_ids) do
          style[style_def.id][style_def.key][1] = alpha
        end

        render_settings.snap_pixel_positions = time_left <= fade_duration

        UIRenderer.draw_widget(ui_renderer, widget)

        render_settings.snap_pixel_positions = snap_pixel_positions
      until true
    end
  end)

  UIRenderer.end_pass(ui_renderer)
end
