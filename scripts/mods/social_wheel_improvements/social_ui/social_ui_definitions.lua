--[[
  Copyright 2020 Lucas Schwiderski

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
--]]

local mod = get_mod("social_wheel_improvements")

-- TODO: Move to mod options
local MAX_NUMBER_OF_MESSAGES = 5
local PORTRAIT_SIZE = { 60, 70 }
local TEXT_WIDTH = 450

local scenegraph_definition = {
  root = {
    scale = "hud_scale_fit",
    position = { 0, 0, UILayer.hud },
    size = { 1920, 1080 },
  },
  message_animated = {
    parent = "root",
    vertical_alignment = "top",
    horizontal_alignment = "right",
    -- The scenegraph node is only as wide as the text is supposed to be, so that word wrap works correctly
    -- The portrait is then moved next to the text via offset
    size = { TEXT_WIDTH, 0 },
    -- Because of the above, the node has to be moved away from the screen edge by the width of the portrait
    -- TODO: Add mod options to move this around
    position = { -(PORTRAIT_SIZE[1] + 5), -500, 1 },
  }
}

local function create_message_widget(scenegraph_id)
  return {
    element = {
      passes = {
        {
          style_id = "text",
          pass_type = "text",
          text_id = "text",
        },
        {
          style_id = "text_shadow",
          pass_type = "text",
          text_id = "text",
          content_check_function = function (content)
            return content.use_shadow
          end,
        },
        {
          pass_type = "texture_uv",
          content_id = "portrait",
          style_id = "portrait",
        },
      },
    },
    content = {
      text = "",
      use_shadow = true,
      portrait = {
        texture_id = "icons_placeholder",
        uvs =  {
          { 0, 0 },
          { 1, 1 },
        },
      },
      texte_style_ids = {
        { id = "portrait", key = "color" },
        { id = "text", key = "text_color" },
        { id = "text_shadow", key = "text_color" },
      },
    },
    style = {
      portrait = {
        color = { 255, 255, 255, 255 },
        size = table.clone(PORTRAIT_SIZE),
        offset = { (TEXT_WIDTH + 5), -53, 3 },
      },
      text = {
        vertical_alignment = "top",
        horizontal_alignment = "right",
        font_size = 28,
        font_type = "hell_shark",
        text_color = Colors.get_color_table_with_alpha("font_title", 255),
        offset = { 0, 0, 2 },
        debug_draw_box = true,
      },
      text_shadow = {
        vertical_alignment = "top",
        horizontal_alignment = "right",
        font_size = 28,
        font_type = "hell_shark",
        text_color = { 255, 0, 0, 0 },
        offset = { 2, -2, 1 },
      },
    },
    offset = { 0, 0, 0 },
    scenegraph_id = scenegraph_id,
  }
end

local message_widgets = {}

for i = 1, MAX_NUMBER_OF_MESSAGES, 1 do
  local widget = create_message_widget("message_animated")
  message_widgets[i] = widget
end

return {
  scenegraph_definition = scenegraph_definition,
  message_widgets = message_widgets,
  MAX_NUMBER_OF_MESSAGES = MAX_NUMBER_OF_MESSAGES,
  PORTRAIT_SIZE = PORTRAIT_SIZE,
  TEXT_WIDTH = TEXT_WIDTH,
}
