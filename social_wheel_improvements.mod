return {
	run = function()
		fassert(rawget(_G, "new_mod"), "social_wheel_improvements must be lower than Vermintide Mod Framework in your launcher's load order.")

		new_mod("social_wheel_improvements", {
			mod_script       = "scripts/mods/social_wheel_improvements/social_wheel_improvements",
			mod_data         = "scripts/mods/social_wheel_improvements/social_wheel_improvements_data",
			mod_localization = "scripts/mods/social_wheel_improvements/social_wheel_improvements_localization"
		})

		return {}
	end,
	packages = {
		"resource_packages/social_wheel_improvements/social_wheel_improvements"
	}
}
